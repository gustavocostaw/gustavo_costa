import ProfilePage from './controllers/profile.js'

document.addEventListener('DOMContentLoaded', function(){
    if(new RegExp("\\b"+"profile"+"\\b").test(window.location.pathname)) {
        new ProfilePage();
    }
})
