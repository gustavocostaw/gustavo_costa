export default class ProfilePage {
    constructor() {
        this.createSwiper();
        this.addEventListeners();
    }

    createSwiper() {
        document.getElementsByClassName('profile-section')[0].style.height = innerHeight - 400+'px';
        this.mySwiper = new Swiper ('.swiper-container', {
            autoplay:false, 
            loop:false,
            onSlideChangeEnd: (swiper) => {
                this.updateActiveClass(this.navItems[swiper.activeIndex]);
            }
        });
    }

    addEventListeners() {
        this.navItems = Array.from(document.getElementsByClassName('nav-item'));
        this.navItems.map( (item,index) => {

            item.addEventListener('click', () => {
                this.updateActiveClass(item);
                this.mySwiper.slideTo(index);
            })
        })
    }

    updateActiveClass(item) {
        this.navItems.map((item) => {
            item.classList.remove('active');
        });

        item.classList.add('active');
    }
}