const gulp = require('gulp');
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const watch = require('gulp-watch');
const batch = require('gulp-batch');
const shell = require('gulp-shell')


gulp.task('minify-html', () => { 
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass', () => { 
        gulp
        .src('src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/css/'));
});

gulp.task('concat-css', () => {
  return gulp.src('src/css/**/**.css')
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('minify-css', () => {
  return gulp.src('dist/css/styles.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/css/prod'));
});

gulp.task('copy', () => {
   gulp.src('src/img/*')
   .pipe(gulp.dest('./dist/img'));
})

gulp.task('watch', () => {
    watch('src/sass/**/*.scss', batch(function (events, done) {
        gulp.start(['sass'], done);
    }));

    watch('src/css/**/*.css', batch(function (events, done) {
        gulp.start(['concat-css'], done);
    }));

    watch('src/**/*.html', batch(function (events, done) {
        gulp.run(['minify-html'], done);
    }));

    /* WEBPACK */
    watch('src/js/**/*.js', batch(function (events, done) {
        gulp.run(['webpack'], done);
    }));
});

// webpack dev
gulp.task('webpack', shell.task('npm run build'));

// webpack prod
gulp.task('webpack-prod', shell.task('npm run build'));


/* DEVELOPMENT */


// Gulp Default Task
gulp.task('default', ['watch']);
// Build
gulp.task('build', ['sass', 'minify-html', 'copy', 'webpack-prod','concat-css', 'minify-css']);